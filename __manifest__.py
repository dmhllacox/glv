# -*- coding: utf-8 -*-
{
    'name': "GLV ERP Instance",

    'summary': """
        Módulos de Grupo Lava Villalobos.
    """,

    'description': """
        Módulos necesarios para instalar el servicio de la instancia de Grupo Lara Villalobos
    """,

    'author': "Llacox",
    'website': "http://www.llacox.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Grupo Lara Villalobos',
    'version': '11.0.1.0.1',

    # any module necessary for this one to work correctly
    'depends': [
        'account',
        'hr_attendance',
        'point_of_sale',
        'product',
        'purchase',
        'stock',
    ],

    # always loaded
    'data': [
        'views/account_invoice_view.xml',
        'views/hr_attendance_view.xml',
        'views/product_template_views.xml',
        'views/purchase_views.xml',
        'views/stock_picking_views.xml',
    ],
    'qweb': ['static/src/xml/pos.xml'],
}