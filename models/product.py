# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class ProductTemplate(models.Model):
    _inherit = 'product.template'
    
    boxes_available = fields.Float('Boxes On Hand', compute='_compute_boxes', default = 0)
    box_track = fields.Boolean(string = 'Rastreo en cajas', help='Permitir rastrear el producto en cajas, además de la unidad de medida por defecto.')
    barcode   = fields.Char('Barcode', store = True, compute = '_compute_barcode')
    
    @api.model
    @api.one
    def _compute_barcode(self):
        self.barcode =  'ABC123ABD'
    
    def _compute_boxes(self):
        variants_available = self.mapped('product_variant_ids')._product_available()
        
        for template in self:
            template.boxes_available += variants_available[template.id]['boxes_qty']

    # def _compute_quantities_dict(self):
    #     # TDE FIXME: why not using directly the function fields ?
    #     variants_available = self.mapped('product_variant_ids')._product_available()
    #     prod_available = {}
    #     for template in self:
    #         qty_available = 0
    #         virtual_available = 0
    #         incoming_qty = 0
    #         outgoing_qty = 0
    #         for p in template.product_variant_ids:
    #             qty_available += variants_available[p.id]["qty_available"]
    #             virtual_available += variants_available[p.id]["virtual_available"]
    #             incoming_qty += variants_available[p.id]["incoming_qty"]
    #             outgoing_qty += variants_available[p.id]["outgoing_qty"]
    #         prod_available[template.id] = {
    #             "qty_available": qty_available,
    #             "virtual_available": virtual_available,
    #             "incoming_qty": incoming_qty,
    #             "outgoing_qty": outgoing_qty,
    #         }
    #     return prod_available