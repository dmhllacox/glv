# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class AccountInvoice(models.Model):
    _inherit = "account.invoice"
    
    exchange_rate = fields.Monetary(string = "Tipo de cambio", compute = '_compute_exchange_rate', readonly = True, store = False)
    
    @api.depends('date_invoice')
    def _compute_exchange_rate(self):
        results = self.env['res.currency.rate'].search([('currency_id', '=', 3),('name', '=', self.date_invoice)])
        if results:
            self.exchange_rate = 1 / results[0].rate