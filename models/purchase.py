# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class PurchaseOrder(models.Model):
    _inherit = "purchase.order"
    
    requirement_number        = fields.Char(string="Número de pedimento")
    mpl_certificate           = fields.Char(string="Certificado MPL")
    animal_health_certificate = fields.Char(string="Certificado zoosanitario")
    
    gross_weight = fields.Float(string="Peso bruto")
    total_kg     = fields.Float(string="Total Kg.")
    
    exchange_rate = fields.Monetary(string="Tipo de cambio", compute='_compute_exchange_rate', readonly=True, store = False)
    usd_price     = fields.Monetary(string="Valor en dólares")
    customs_value = fields.Monetary(string="Valor aduana")
    
    def _compute_exchange_rate(self):
        results = self.env['res.currency.rate'].search([('currency_id', '=', 3)])
        if results:
            self.exchange_rate = 1 / results[-1].rate
    
class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'
    
    boxes_qty = fields.Integer(string="Total Cajas")