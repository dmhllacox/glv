# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class StockQuant(models.Model):
    _inherit = 'stock.quant'
    
    boxes = fields.Integer(string = 'Cajas')