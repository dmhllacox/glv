# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class StockMove(models.Model):
    _inherit = "stock.move"
    
    boxes_qty = fields.Integer(string="Total Cajas")