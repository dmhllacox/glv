# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
    
class HrAttendance(models.Model):
    _inherit = 'hr.attendance'
    
    worked_hours_time = fields.Char(string='Worked Hours', compute='_compute_worked_hours_time')
    
    @api.one
    @api.depends('worked_hours')
    def _compute_worked_hours_time(self):
        i, d = divmod(self.worked_hours, 1)
        self.worked_hours_time = '{}:{}'.format(str(int(i)).zfill(2) , str(int(d * 60)).zfill(2) )