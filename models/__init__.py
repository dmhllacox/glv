# -*- coding: utf-8 -*-

from . import account_invoice
from . import hr_attendance
from . import product
from . import purchase
from . import stock_move
from . import stock_picking
from . import stock_quant