# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class StockPicking(models.Model):
    _inherit = 'stock.picking'
    
    invoice_number = fields.Char(string="Numero de Factura")
    amount_usd     = fields.Float(string="Valor USD", digits=(8,2))
    aduana_value   = fields.Float(string="Valor Aduana", digits=(8,2))
    supplier_id    = fields.Many2one('res.partner', string="Proveedor")
    certify_mpl    = fields.Char(string="Certificado MPL")
    certify_zoo    = fields.Char(string="Certificado Zoosnaitario")